FROM archlinux:latest

MAINTAINER Filip Gralinski <filipg@amu.edu.pl>

USER root

RUN pip install -r requirements.txt

RUN pacman -Syu --noconfirm && pacman --noconfirm -S texlive-most python-jinja make && pacman --noconfirm -Scc
